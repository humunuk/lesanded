DROP DATABASE IF EXISTS test_task;

CREATE DATABASE test_task;

USE test_task;

CREATE TABLE patient (
  _id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pn VARCHAR(11) DEFAULT NULL,
  first VARCHAR(15) DEFAULT NULL,
  last VARCHAR(25) DEFAULT NULL,
  dob DATE DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE insurance (
  _id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  patient_id INT(10) UNSIGNED NOT NULL,
  iname VARCHAR(40) DEFAULT NULL,
  from_date DATE DEFAULT NULL,
  to_date DATE DEFAULT NULL,
  CONSTRAINT patient_fk_id
  FOREIGN KEY patient_fk (patient_id)
  REFERENCES patient (_id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
) ENGINE=InnoDB;

INSERT INTO patient (pn, first, last, dob) VALUES
('0000000001', 'Taavi', 'Trapets', '1967-10-23'),
('0000000002', 'Mari', 'Murakas', '1998-01-12'),
('0000000003', 'John', 'Doe', '2012-03-18'),
('0000000004', 'Eduardo', 'Iglejases', '2000-09-01'),
('0000000005', 'Gustav', 'Koronger', '2001-08-30');

INSERT INTO insurance (patient_id, iname, from_date, to_date) VALUES
('1', 'WonderLand', '2010-10-12', '2020-01-01'),
('2', 'SeepJaCare', '2011-01-12', '2013-11-18'),
('3', 'WonderLand', '2012-01-15', '2020-01-01'),
('4', 'SeepJaCare', '2013-08-12', '2020-01-01'),
('5', 'WonderLand', '2000-09-17', '2013-08-12'),
('1', 'SeepJaCare', '2014-10-12', '2020-01-01'),
('2', 'WonderLand', '2001-10-12', '2007-12-12'),
('3', 'SeepJaCare', '2005-12-12', '2050-04-01'),
('4', 'WonderLand', '2005-10-23', '2080-04-01'),
('5', 'SeepJaCare', '2011-01-12', '2020-07-01');
