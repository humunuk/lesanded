<?php

/*
 * Exercise 3
 * by Siim Kallari, siimkallari@gmail.com
 *
 */

namespace Exercise3;

use PDO;
use DateTime;

class Insurance implements PatientRecord {

  //Properties

  protected $id = NULL;
  protected $pn = NULL;
  protected $db;

  //Methods

  function __construct(PDO $db, $id) {
    $this->db = $db;
    $this->id = $id;
  }

  /*
   * Returns implementing record's ID
   * @return (int) ID
   */

  public function getID() {

    return $this->id;

  }

  /*
   * Returns implementing record's patient number
   * @return string PN
   */

  public function getPN() {

    $stmt = $this->db->prepare("SELECT pn FROM patient WHERE _id = '$this->id'");
    $stmt->execute();
    $this->pn = $stmt->fetchColumn();

    return $this->pn;

  }

  /*
   * Returns implementing record's insurance status
   * @param date in format of MM-DD-YY
   * @return true or false
   */

  public function getInsuranceStatus($date) {

    $id = $this->id;

    $dateFormat = DateTime::createFromFormat('m-d-y', $date);
    $compareDate = $dateFormat->format('Y-m-d');

    $stmt = $this->db->prepare("SELECT patient_id,
        (CASE WHEN from_date <= '$compareDate' AND (to_date >= '$compareDate' OR to_date IS NULL)
         THEN 'true' ELSE 'false' END) as Status
         FROM insurance WHERE patient_id = '$id'");
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($results as $result) {
      $insuranceStatus[] = $result['Status'];
    }
    return implode(",", $insuranceStatus);
  }
}
