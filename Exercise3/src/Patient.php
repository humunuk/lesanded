<?php

/*
 * Exercise 3 for Raintree OÜ
 * by Siim Kallari, siimkallari@gmail.com
 *
 */

namespace Exercise3;

use PDO;
use DateTime;

class Patient implements PatientRecord {

  // Properties

  protected $id = NULL;
  protected $dob = NULL;
  protected $insurance = array();
  protected $db;
  public $first = '';
  public $last = '';

  // Methods

  function __construct(PDO $db, $pn) {

    $this->db = $db;
    $this->pn = $pn;

  }

  /*
   * Returns implementing record's ID
   * @return (int) ID
   */

  public function getID() {

    $stmt = $this->db->prepare("SELECT _id FROM patient WHERE pn = '$this->pn'");
    $stmt->execute();
    $this->_id = $stmt->fetchColumn();

    return $this->_id;

  }

  /*
   * Returns implementing record's patient number
   * @return string PN
   */

  public function getPN() {

    return $this->pn;

  }

  /*
   * Returns implementing record's first and last names
   * @return string First name and Last name
   */

  public function getPatientName() {

    $stmt = $this->db->prepare("SELECT first, last FROM patient WHERE pn = '$this->pn'");
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($results as $result) {
      return "".$result['first']." ".$result['last']."";
    }

  }

  /*
   * Returns implementing record's insurance information
   * @return array
   */

  public function getInsurance() {

    $id = $this->getID();

    $stmt = $this->db->prepare("SELECT iname, from_date, to_date FROM insurance WHERE patient_id = '$id'");
    $stmt->execute();
    $this->insurance = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $this->insurance;
  }

  /*
   * Returns implementing record's insurance status
   * @param date in format of MM-DD-YY
   * @return string PN, Name, Yes/No
   */

  public function getInsuranceStatus($date) {

    $name = $this->getPatientName();
    $insurances = $this->getInsurance();

    $dateFormat = DateTime::createFromFormat('m-d-y', $date);
    $dateFormat->format('Y-m-d');

    $mask = "|%12s|%-30s|%-25s|%12s|" . PHP_EOL;

    foreach ($insurances as $insurance) {

      $fromDate = new DateTime($insurance['from_date']);
      $toDate = new DateTime($insurance['to_date']);

      if ($dateFormat >= $fromDate && $dateFormat <= $toDate) {
        printf($mask, $this->pn, $name, $insurance['iname'], "Yes") . PHP_EOL;
      } else {
        printf($mask, $this->pn, $name, $insurance['iname'], "No") . PHP_EOL;
      }
    }


  }
}
