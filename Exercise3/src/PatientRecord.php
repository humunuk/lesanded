<?php

/*
 * Exercise 3 for Raintree OÜ
 * by Siim Kallari, siimkallari@gmail.com
 *
 */

namespace Exercise3;

interface PatientRecord {

  public function getID();
  public function getPN();
}
