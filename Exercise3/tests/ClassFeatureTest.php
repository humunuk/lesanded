<?php

/*
 * Exercise 3
 * by Siim Kallari, siimkallari@gmail.com
 *
 */

require '../vendor/autoload.php';

use Exercise3\Patient;
use Exercise3\Insurance;

// Database information

$dsn = "";
$user = "";
$pass = "";

try {
$db = new PDO($dsn, $user, $pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo $e->getMessage();
}

//Values to be tested

$pn = '0000000001';
$id = 1;
$date = '01-01-05';

header('Content-type: text/plain; charset=utf8');

// Test for Patient class to get patients ID, First Last names and patient number

$testPatient = new Patient($db, $pn);
$mask = "|%12s|%-25s|%12s|" . PHP_EOL;
printf($mask, 'ID', 'First Last', 'PN');
printf($mask, $testPatient->getID(), $testPatient->getPatientName(), $testPatient->getPN());
printf($mask, '-', '-', '-');

// Test to get associated patients Insurance information

$insurances = $testPatient->getInsurance();

$mask = "|%12s|%-25s|%12s|" . PHP_EOL;
printf($mask, 'Iname', 'From Date', 'To Date');
foreach ($insurances as $insurance) {
  printf($mask, $insurance['iname'], $insurance['from_date'], $insurance['to_date']);
}

printf($mask, '-', '-', '-');

// Test to get associated patients status of insurance

$mask = "|%12s|%-30s|%-25s|%12s|" . PHP_EOL;
printf($mask, 'PN', 'First Last', 'Insurance', 'Is Valid');

$isValid = $testPatient->getInsuranceStatus($date);

$testInsurance = new Insurance($db, $id);
$mask = "|%5s|%-12s| %-12s|" . PHP_EOL;
printf($mask, 'ID', 'PN', 'Insurance?');
printf($mask, $testInsurance->getID(), $testInsurance->getPN(), $testInsurance->getInsuranceStatus($date));
