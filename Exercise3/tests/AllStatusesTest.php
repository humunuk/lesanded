<?php
/*
 * Exercise 3
 * by Siim Kallari, siimkallari@gmail.com
 *
 */

require '../vendor/autoload.php';

use Exercise3\Patient;

// Database information

$dsn = "";
$user = "";
$pass = "";

try {
$db = new PDO($dsn, $user, $pass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo $e->getMessage();
}

// Prints out all patients in database and checks if insurance is valid

$date = new DateTime();
$today = $date->format('m-d-y');

$stmt = $db->prepare("SELECT pn FROM patient");
$stmt->execute();
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

header('Content-type: text/plain; charset=utf8');

$mask = "|%12s|%-30s|%-25s|%12s|" . PHP_EOL;
printf($mask, 'PN', 'First Last', 'Insurance', 'Is Valid');

foreach ($results as $result) {
  $status = new Patient($db, $result['pn']);
  $status->getInsuranceStatus($today);
}
