<?php

/*
 * Exercise 2
 * by Siim Kallari, siimkallari@gmail.com
 *
 */

// Parameters for database connection

$user = '';
$password = '';
$host = '';
$database = '';

// Database connection

try {
$db = new PDO("mysql:host=$host;dbname=$database", $user, $password);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
  echo $db . "\n" . $e->getMessage();
}

// Query for the first part of exercise

try {
$query = 'SELECT pn, last, first, iname, DATE_FORMAT(from_date, "%m-%d-%y") AS FromDate, DATE_FORMAT(to_date, "%m-%d-%y") AS ToDate FROM insurance JOIN patient ON patient._id = insurance.patient_id WHERE patient_id ORDER BY from_date ASC';
$results = $db->prepare($query);
$results->execute();
} catch (PDOException $e) {
  echo $e->getMessage();
}

// Output for first part of exercise

header('Content-type: text/plain');

foreach ($results as $result) {
  echo "".$result['pn'].", ".$result['last'].", ".$result['first'].", ".$result['FromDate'].", ".$result['ToDate'] . PHP_EOL;
}

echo PHP_EOL;

// Query for second part of exercise

try {
  $query = 'SELECT first, last FROM patient';
  $stmt = $db->prepare($query);
  $stmt->execute();
  $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
  echo $e->getMessage();
}

// Output for second part of exercise

foreach ($results as $result) {
  $list[] = $result['first'] . $result['last']; // Join SQL query object into an array
}

$list2 = strtoupper(implode('', $list)); // Convert characters into uppercase and join array elements into string
$length = strlen(preg_replace("/[^A-Za-z]/","",$list2)); // Remove spaces, non alphabetical characters from string, calculate length for percentage calculation

/*
 * $i character in ASCII
 * $val number value of occurance for specific character in a string
 */

foreach (count_chars(preg_replace("/[^A-Za-z]/", "", $list2), 1) as $i => $val) {
  echo chr($i) . "\t" . $val . "\t" . round($val/$length*100, 2) . "%" . PHP_EOL;
}
